package com.macro.mall.tiny.domin;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

//部门表
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Department {

    private Integer id;
    private String departmentName;
}
