package com.macro.mall.tiny.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainPageController {
    @RequestMapping({"/charts","/charts.html"})
    public String charts(){
        return "charts";
    }
    @RequestMapping({"/tables","/tables.html"})
    public String tables(){
        return "tables";
    }

    @RequestMapping({"/forms","/forms.html"})
    public String forms(){
        return "forms";
    }
    @RequestMapping({"/login","login.html"})
    public String login(){
        return "login";
    }

    @RequestMapping({"/test","test.html"})
    public String test(){
        return "test";
    }
}
