package com.macro.mall.tiny.dao;


import com.macro.mall.tiny.domin.Department;
import com.macro.mall.tiny.domin.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

//员工dao
@Repository
public class EmployeeDao {

    private static Map<Integer, Employee> employeeMap=null;
    //模拟数据库中的数据
    //员工有所属的部门
    @Autowired
    private DepartmentDao departmentDao;
    static {
        employeeMap = new HashMap<>();//创建一个员工表


        employeeMap.put(101,new Employee(1001,"AA","483489763@qq.com",0,new Department(101,"后勤部")));
        employeeMap.put(102,new Employee(1002,"BB","483489763@qq.com",1,new Department(101,"教学部")));
        employeeMap.put(103,new Employee(1003,"CC","483489763@qq.com",0,new Department(101,"教研部")));
        employeeMap.put(104,new Employee(1004,"DD","483489763@qq.com",1,new Department(101,"运营部")));
        employeeMap.put(105,new Employee(1005,"EE","483489763@qq.com",0,new Department(101,"市场部")));

    }


    //获得所有员工信息
    public Collection<Employee> getDepartments(){
        return employeeMap.values();
    }

    //通过id得到员工
    public Employee getDepartmentsById(Integer id){
        return employeeMap.get(id);
    }

    //主键自增
    private static Integer initID=1006;
    //增加一个员工
    public void save(Employee employee){
        try {
            if (employee.getId()==null){
                employee.setId(initID);
            }
            employee.setDepartment(departmentDao.getDepartmentsById(employee.getDepartment().getId()));

            employeeMap.put(employee.getId(),employee);
            initID++;
        } catch (Exception e) {
            initID--;
            e.printStackTrace();
        }
    }

    public void delete(Integer id){
        employeeMap.remove(id);
    }
}
